﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abyss
{
    public interface ILocationsFeatures
    {
        int Feature(LocationsPool id);
    }
}
