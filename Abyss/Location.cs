﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abyss
{
    public enum LocationsPool
    {
        location1=1,
        location2=2,
        location3=3
    }
    public class Location
    {
        public LocationsPool Id { get; private set; }
        public string Name { get; private set; }
        public string Describtion { get; private set; }

        public Location(LocationsPool id,string name,string describtion)
        {
            Id = id;
            Name = name;
            Describtion = describtion;
        }

        public int Feature(ILocationsFeatures skill)
        {
           return skill.Feature(Id);
        }
        public override string ToString()
        {
            return Describtion;
        }
    }
}
