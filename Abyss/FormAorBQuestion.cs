﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Abyss
{
    public partial class FormAorBQuestion : Form
    {
        public bool ReturnValue { get; set; }

        public FormAorBQuestion()
        {
            InitializeComponent();
        }

        private void buttonPearl_Click(object sender, EventArgs e)
        {
            ReturnValue = true;
            DialogResult = DialogResult.OK;
        }

        private void buttonToken_Click(object sender, EventArgs e)
        {
            ReturnValue = false;
            DialogResult = DialogResult.OK;
        }
    }
}
