﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abyss
{
    public enum RaceOption
    {
        jellyfish,
        crabs,
        seahorses,
        shellfish,
        squids,
        monster
    }
    
    public class Game
    {
        public List<Player> AllPlayers;
        public Player ActivePlayer;
        public List<Ally> AllyStack;
        public List<Lord> LordStack;
        public List<Location> LocationStack;
        public List<Location> PublicLocationsPool;
        public List<Lord> Court;
        public List<Ally>[] Council;
        public List<Ally> ExplorationTrack;
        public int AllyCost;
        public DangerTrack DangerTrack;
        public List<Ally> DiscardedAllies;
        public List<Lord> DiscardedLords;

        public Game(List<Player> players, IDecksGenerator deckGenerator)
        {
            AllPlayers = players;
            AllyStack = deckGenerator.GetAllyStack();
            LordStack = deckGenerator.GetLordsStack();
            LocationStack = deckGenerator.GetLocationStack();
            ActivePlayer = AllPlayers[0];
            DangerTrack = new DangerTrack();
            Court = new List<Lord>();
            Council = new List<Ally>[5];
            ExplorationTrack = new List<Ally>();
            DiscardedAllies = new List<Ally>();
            DiscardedLords = new List<Lord>();
            PublicLocationsPool = new List<Location>();
            Court.Capacity = 6;
            ExplorationTrack.Capacity = 5;
            AllyCost = 1;
        }

        public void Shuffle(List<object> stack)
        {
            var rand = new Random();
            int id;
            var shuffledDeck = new List<object>();
            while (stack.Count != 0)
            {
                id=rand.Next(0, stack.Count);
                shuffledDeck.Add(stack[id]);
                stack.RemoveAt(id);
            }
            stack = shuffledDeck;
        }
    }
}
