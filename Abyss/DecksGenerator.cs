﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Abyss
{
    public class DecksGenerator:IDecksGenerator
    {
          
        public List<Lord> GetLordsStack()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory+"\\lords.json";
            var jsonBack = File.ReadAllText(path);
            var jsonRead = JsonConvert.DeserializeObject<List<Lord>>(jsonBack);

            return jsonRead;
        }
                      
        public List<Ally> GetAllyStack()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + "\\allies.json";
            var jsonBack = File.ReadAllText(path);
            var jsonRead = JsonConvert.DeserializeObject<List<Ally>>(jsonBack);

            return jsonRead;
        }

        public List<Location> GetLocationStack()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + "\\locations.json";
            var jsonBack = File.ReadAllText(path);
            var jsonRead = JsonConvert.DeserializeObject<List<Location>>(jsonBack);

            return jsonRead;
        }
    }
}
