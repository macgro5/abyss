﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Abyss
{
    public partial class FormGame : Form
    {
        public Game Game;

        public FormGame(Game game)
        {
            InitializeComponent();
            Game = game;
            groupBoxPlayer.Text= Game.ActivePlayer.PlayerName + "'s resources:";
            labelPearls.Text = Game.ActivePlayer.Pearls.ToString();
        }
        private void Form2_Load(object sender, EventArgs e)
        {
           
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Game.ActivePlayer.EndTurn();
            groupBoxPlayer.Text = Game.ActivePlayer.PlayerName + "'s resources:";
        }
        public void OnExplorationTrackChange(object sender, EventArgs e)
        {
            pictureBoxEx0.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyEx0.jpg");
            pictureBoxEx1.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyEx1.jpg");
            pictureBoxEx2.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyEx2.jpg");
            pictureBoxEx3.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyEx3.jpg");
            pictureBoxEx4.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyEx4.jpg");

            foreach (Ally card in Game.ExplorationTrack)
            switch (Game.ExplorationTrack.IndexOf(card))
            {
                case 0:
                    pictureBoxEx0.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                    break;
                case 1:
                    pictureBoxEx1.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                    break;
                case 2:
                    pictureBoxEx2.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                    break;
                case 3:
                    pictureBoxEx3.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                    break;
                case 4:
                    pictureBoxEx4.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                    break;
            }
        }
        public void OnCourtChange(object sender, EventArgs e)
        {
            pictureBoxCourt0.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyCourt0.jpg");
            pictureBoxCourt1.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyCourt1.jpg");
            pictureBoxCourt2.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyCourt2.jpg");
            pictureBoxCourt3.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyCourt3.jpg");
            pictureBoxCourt4.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyCourt4.jpg");
            pictureBoxCourt5.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\emptyCourt5.jpg");
            foreach(Lord card in Game.Court)
                switch (Game.Court.IndexOf(card))
                {
                    case 0:
                        pictureBoxCourt0.BackgroundImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 1:
                        pictureBoxCourt1.BackgroundImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 2:
                        pictureBoxCourt2.BackgroundImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 3:
                        pictureBoxCourt3.BackgroundImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 4:
                        pictureBoxCourt4.BackgroundImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 5:
                        pictureBoxCourt5.BackgroundImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                }
        }
        public void OnCouncilChange(object sender, EventArgs e)
        {
            labelCouncil0.Text = Game.Council[0].Count.ToString();
            labelCouncil1.Text = Game.Council[1].Count.ToString();
            labelCouncil2.Text = Game.Council[2].Count.ToString();
            labelCouncil3.Text = Game.Council[3].Count.ToString();
            labelCouncil4.Text = Game.Council[4].Count.ToString();
        }
        public void OnPlayersLordChange(object sender,EventArgs e)
        {
            pictureBoxLord0.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\LordBack4.jpg");
            pictureBoxLord1.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\LordBack4.jpg");
            pictureBoxLord2.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\LordBack4.jpg");
            pictureBoxLord3.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\LordBack4.jpg");
            pictureBoxLord4.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\LordBack4.jpg");
            pictureBoxLord5.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\LordBack4.jpg");
            pictureBoxLord6.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\LordBack4.jpg");
            pictureBoxLord7.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\LordBack4.jpg");

            foreach (Lord card in Game.ActivePlayer.PlayersLords)
                switch (Game.ActivePlayer.PlayersLords.IndexOf(card))
                {
                    case 0:
                        pictureBoxLord0.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 1:
                        pictureBoxLord1.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 2:
                        pictureBoxLord2.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 3:
                        pictureBoxLord3.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 4:
                        pictureBoxLord4.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 5:
                        pictureBoxLord5.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 6:
                        pictureBoxLord6.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 7:
                        pictureBoxLord7.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                }
        }
        public void OnPlayersAffiliatedAlliesChange(object sender,EventArgs e)
        {
            pictureBoxAlly0.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\AllyBack.jpg");
            pictureBoxAlly1.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\AllyBack.jpg");
            pictureBoxAlly2.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\AllyBack.jpg");
            pictureBoxAlly3.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\AllyBack.jpg");
            pictureBoxAlly4.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\AllyBack.jpg");
            pictureBoxAlly5.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\AllyBack.jpg");
            pictureBoxAlly6.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\AllyBack.jpg");
            pictureBoxAlly7.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\AllyBack.jpg");

            foreach (Ally card in Game.ActivePlayer.AffiliatedAllies)
                switch (Game.ActivePlayer.AffiliatedAllies.IndexOf(card))
                {
                    case 0:
                        pictureBoxAlly0.Image= Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 1:
                        pictureBoxAlly1.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 2:
                        pictureBoxAlly2.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 3:
                        pictureBoxAlly3.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 4:
                        pictureBoxAlly4.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 5:
                        pictureBoxAlly5.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 6:
                        pictureBoxAlly6.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;
                    case 7:
                        pictureBoxAlly7.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + card.Name + ".jpg");
                        break;

                }
        }
        public void OnPlayersPearlsChange(object sender,EventArgs e)
        {
            labelPearls.Text = Game.ActivePlayer.Pearls.ToString();
        }
        public void OnPlayersKeysChange(object sender, EventArgs e)
        {
            labelKeys.Text = Game.ActivePlayer.PlayersKeys.ToString();
        }
        public void OnPlayersTokensChange(object sender, EventArgs e)
        {
            listBoxTokens.DataSource = Game.ActivePlayer.PlayersMonsterTokens;
        }
        public void OnPlayersLocationsChange(object sender,EventArgs e)
        {
            listBoxLocations.DataSource = Game.ActivePlayer.PlayersLocations;
        }
        public void OnPlayersHandChange(object sender,EventArgs e)
        {
            listBoxHand.DataSource = Game.ActivePlayer.Hand;
        }

        private void pictureExplore_Click(object sender, EventArgs e)
        {
            Game.ActivePlayer.Explore();
        }

        private void picturePlotAtCourt_Click(object sender, EventArgs e)
        {
            try { Game.ActivePlayer.AddLordToCourt(); }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBoxCouncil0_Click(object sender, EventArgs e)
        {

        }
    }
}
