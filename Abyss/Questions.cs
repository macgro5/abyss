﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace Abyss
{
    public class Questions:IQuestions
    {
        public bool YesNoQuestion(string question, string windowName) //Done
        {
            DialogResult result = MessageBox.Show(question,windowName,MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
                return true;
            else
                return false;
        }
        public bool PearlOrTokenQuestion(string playerName) //Done
        {
            var form = new FormAorBQuestion();
            form.Text = "Player " + playerName + " is fighting the monster!";
            form.labelQuestion.Text = "Do you (" + playerName + ") take a pearl or a monster token?";
            form.buttonTrue.Text = "Pearl";
            form.buttonFalse.Text = "Monster Token";
            var result=form.ShowDialog();

            if (result == DialogResult.OK)
            {
                var flag = form.ReturnValue;
                form.Close();
                return flag;
            }
            else
                throw new SystemException();

        }
        public bool AvailableOrStackLocations(string playerName) //Done
        {
            var form = new FormAorBQuestion();
            form.Text = "Player " + playerName + " is choosing location";
            form.labelQuestion.Text = "Do you (" + playerName + ") want to chose new location from Available locations or from the stack?";
            form.buttonTrue.Text = "Available Locations";
            form.buttonFalse.Text = "Locations' Stack";
            var result = form.ShowDialog();

            if (result == DialogResult.OK)
            {
                var flag = form.ReturnValue;
                form.Close();
                return flag;
            }
            else
                throw new SystemException();
        }
        public Ally AllyToAffiliate(List<Ally> allies) //Done
        {
            var form = new FormListToChoose();
            form.labelQuestion.Text = "Choose ally to affiliate:";
            form.buttonConfirm.Text = "Choose ally";
            form.Text = "Abyss";
            form.listBox1.SelectionMode = SelectionMode.One;
            form.listBox1.DataSource = allies;
            var result = form.ShowDialog();

            if (result == DialogResult.OK)
            {
                var flag = (Ally)form.ReturnValue;
                form.Close();
                return flag;
            }
            else
                throw new SystemException();
        }
        public Location LocationToChose(List<Location> listOfLocations) //Done
        {
            var form = new FormListToChoose();
            form.labelQuestion.Text = "Choose location:";
            form.buttonConfirm.Text = "Choose location";
            form.Text = "Abyss";
            form.listBox1.SelectionMode = SelectionMode.One;
            form.listBox1.DataSource = listOfLocations;
            var result = form.ShowDialog();

            if (result == DialogResult.OK)
            {
                var flag = (Location)form.ReturnValue;
                form.Close();
                return flag;
            }
            else
                throw new SystemException();
        }
        public List<Lord>ChoseLordsForLocation(List<Lord> lordsToChose,int keysRequired) //Done
        {
            var isCorrect = false;
            var resultanList = new List<Lord>();
            while (isCorrect == false)
            {
                var form = new FormListToChoose();
                form.labelQuestion.Text = "Choose lords to occupy location:";
                form.buttonConfirm.Text = "Choose lords";
                form.Text = "Abyss";
                form.listBox1.SelectionMode = SelectionMode.MultiSimple;
                form.listBox1.DataSource = lordsToChose;
                var result = form.ShowDialog();

                if (result == DialogResult.OK)
                {
                    resultanList = (List<Lord>)form.ReturnValue;
                    form.Close();
                }
                else
                    throw new SystemException();
                var resultanKeys = 0;
                foreach (Lord lord in resultanList)
                {
                    resultanKeys += lord.Keys;
                }
                if (resultanKeys == keysRequired)
                    isCorrect = true;
                else
                    MessageBox.Show("Incorrect number of keys (Required "+keysRequired+"). Try again.");
            }
            return resultanList;
        }
        public int HowManyLocsFromStack(string playersName) //Done
        {
            var form = new FormListToChoose();
            form.labelQuestion.Text = "How many locations you ("+playersName+") wish to draw?";
            form.buttonConfirm.Text = "Draw locations";
            form.Text = "Abyss";
            form.listBox1.SelectionMode = SelectionMode.One;
            form.listBox1.DataSource = new List<int> { 1, 2, 3, 4 };
            var result = form.ShowDialog();

            if (result == DialogResult.OK)
            {
                var flag = (int)form.ReturnValue;
                form.Close();
                return flag;
            }
            else
                throw new SystemException();
        }
    }
}
