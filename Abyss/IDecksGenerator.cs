﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Abyss
{
    public interface IDecksGenerator
    {
        
        List<Lord> GetLordsStack();
        
        List<Ally> GetAllyStack();
        
        List<Location> GetLocationStack();
    }
}
