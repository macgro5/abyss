﻿namespace Abyss
{
    partial class FormGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGame));
            this.groupBoxBoard = new System.Windows.Forms.GroupBox();
            this.labelCouncil4 = new System.Windows.Forms.Label();
            this.labelCouncil3 = new System.Windows.Forms.Label();
            this.labelCouncil2 = new System.Windows.Forms.Label();
            this.labelCouncil1 = new System.Windows.Forms.Label();
            this.labelCouncil0 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxTokens = new System.Windows.Forms.PictureBox();
            this.buttonEndTurn = new System.Windows.Forms.Button();
            this.pictureBoxLocAv = new System.Windows.Forms.PictureBox();
            this.pictureBoxLocStack = new System.Windows.Forms.PictureBox();
            this.pictureBoxCourt0 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCourt1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCourt2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCourt3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCourt4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCourt5 = new System.Windows.Forms.PictureBox();
            this.pictureBoxEx4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxEx3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxEx2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxEx1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxEx0 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCouncil4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCouncil3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCouncil2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCouncil1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCouncil0 = new System.Windows.Forms.PictureBox();
            this.pictureExplore = new System.Windows.Forms.PictureBox();
            this.picturePlotAtCourt = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxBoard = new System.Windows.Forms.PictureBox();
            this.groupBoxPlayer = new System.Windows.Forms.GroupBox();
            this.groupBoxTokens = new System.Windows.Forms.GroupBox();
            this.groupBoxKeys = new System.Windows.Forms.GroupBox();
            this.labelKeys = new System.Windows.Forms.Label();
            this.groupBoxPearls = new System.Windows.Forms.GroupBox();
            this.labelPearls = new System.Windows.Forms.Label();
            this.groupBoxAllies = new System.Windows.Forms.GroupBox();
            this.pictureBoxAlly7 = new System.Windows.Forms.PictureBox();
            this.pictureBoxAlly6 = new System.Windows.Forms.PictureBox();
            this.pictureBoxAlly5 = new System.Windows.Forms.PictureBox();
            this.pictureBoxAlly4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxAlly3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxAlly2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxAlly1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxAlly0 = new System.Windows.Forms.PictureBox();
            this.groupBoxLocations = new System.Windows.Forms.GroupBox();
            this.pictureBoxLoc0 = new System.Windows.Forms.PictureBox();
            this.groupBoxLords = new System.Windows.Forms.GroupBox();
            this.labelLord7 = new System.Windows.Forms.Label();
            this.labelLord6 = new System.Windows.Forms.Label();
            this.labelLord5 = new System.Windows.Forms.Label();
            this.labelLord4 = new System.Windows.Forms.Label();
            this.labelLord3 = new System.Windows.Forms.Label();
            this.labelLord2 = new System.Windows.Forms.Label();
            this.labelLord1 = new System.Windows.Forms.Label();
            this.labelLord0 = new System.Windows.Forms.Label();
            this.pictureBoxLord7 = new System.Windows.Forms.PictureBox();
            this.pictureBoxLord6 = new System.Windows.Forms.PictureBox();
            this.pictureBoxLord5 = new System.Windows.Forms.PictureBox();
            this.pictureBoxLord4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxLord3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxLord2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxLord1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxLord0 = new System.Windows.Forms.PictureBox();
            this.listBoxTokens = new System.Windows.Forms.ListBox();
            this.groupBoxHand = new System.Windows.Forms.GroupBox();
            this.listBoxLocations = new System.Windows.Forms.ListBox();
            this.listBoxHand = new System.Windows.Forms.ListBox();
            this.groupBoxBoard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTokens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLocAv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLocStack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEx4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEx3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEx2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEx1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEx0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCouncil4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCouncil3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCouncil2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCouncil1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCouncil0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureExplore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturePlotAtCourt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBoard)).BeginInit();
            this.groupBoxPlayer.SuspendLayout();
            this.groupBoxTokens.SuspendLayout();
            this.groupBoxKeys.SuspendLayout();
            this.groupBoxPearls.SuspendLayout();
            this.groupBoxAllies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly0)).BeginInit();
            this.groupBoxLocations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoc0)).BeginInit();
            this.groupBoxLords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord0)).BeginInit();
            this.groupBoxHand.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxBoard
            // 
            this.groupBoxBoard.Controls.Add(this.labelCouncil4);
            this.groupBoxBoard.Controls.Add(this.labelCouncil3);
            this.groupBoxBoard.Controls.Add(this.labelCouncil2);
            this.groupBoxBoard.Controls.Add(this.labelCouncil1);
            this.groupBoxBoard.Controls.Add(this.labelCouncil0);
            this.groupBoxBoard.Controls.Add(this.pictureBox4);
            this.groupBoxBoard.Controls.Add(this.pictureBoxTokens);
            this.groupBoxBoard.Controls.Add(this.buttonEndTurn);
            this.groupBoxBoard.Controls.Add(this.pictureBoxLocAv);
            this.groupBoxBoard.Controls.Add(this.pictureBoxLocStack);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCourt0);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCourt1);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCourt2);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCourt3);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCourt4);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCourt5);
            this.groupBoxBoard.Controls.Add(this.pictureBoxEx4);
            this.groupBoxBoard.Controls.Add(this.pictureBoxEx3);
            this.groupBoxBoard.Controls.Add(this.pictureBoxEx2);
            this.groupBoxBoard.Controls.Add(this.pictureBoxEx1);
            this.groupBoxBoard.Controls.Add(this.pictureBoxEx0);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCouncil4);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCouncil3);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCouncil2);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCouncil1);
            this.groupBoxBoard.Controls.Add(this.pictureBoxCouncil0);
            this.groupBoxBoard.Controls.Add(this.pictureExplore);
            this.groupBoxBoard.Controls.Add(this.picturePlotAtCourt);
            this.groupBoxBoard.Controls.Add(this.pictureBox1);
            this.groupBoxBoard.Controls.Add(this.pictureBoxBoard);
            this.groupBoxBoard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBoxBoard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxBoard.Location = new System.Drawing.Point(13, 13);
            this.groupBoxBoard.Name = "groupBoxBoard";
            this.groupBoxBoard.Size = new System.Drawing.Size(1156, 1037);
            this.groupBoxBoard.TabIndex = 0;
            this.groupBoxBoard.TabStop = false;
            this.groupBoxBoard.Text = "Board";
            // 
            // labelCouncil4
            // 
            this.labelCouncil4.AutoSize = true;
            this.labelCouncil4.Location = new System.Drawing.Point(839, 434);
            this.labelCouncil4.Name = "labelCouncil4";
            this.labelCouncil4.Size = new System.Drawing.Size(19, 20);
            this.labelCouncil4.TabIndex = 29;
            this.labelCouncil4.Text = "0";
            // 
            // labelCouncil3
            // 
            this.labelCouncil3.AutoSize = true;
            this.labelCouncil3.Location = new System.Drawing.Point(700, 367);
            this.labelCouncil3.Name = "labelCouncil3";
            this.labelCouncil3.Size = new System.Drawing.Size(19, 20);
            this.labelCouncil3.TabIndex = 28;
            this.labelCouncil3.Text = "0";
            // 
            // labelCouncil2
            // 
            this.labelCouncil2.AutoSize = true;
            this.labelCouncil2.Location = new System.Drawing.Point(495, 344);
            this.labelCouncil2.Name = "labelCouncil2";
            this.labelCouncil2.Size = new System.Drawing.Size(19, 20);
            this.labelCouncil2.TabIndex = 27;
            this.labelCouncil2.Text = "0";
            // 
            // labelCouncil1
            // 
            this.labelCouncil1.AutoSize = true;
            this.labelCouncil1.Location = new System.Drawing.Point(281, 367);
            this.labelCouncil1.Name = "labelCouncil1";
            this.labelCouncil1.Size = new System.Drawing.Size(19, 20);
            this.labelCouncil1.TabIndex = 26;
            this.labelCouncil1.Text = "0";
            // 
            // labelCouncil0
            // 
            this.labelCouncil0.AutoSize = true;
            this.labelCouncil0.Location = new System.Drawing.Point(135, 434);
            this.labelCouncil0.Name = "labelCouncil0";
            this.labelCouncil0.Size = new System.Drawing.Size(19, 20);
            this.labelCouncil0.TabIndex = 25;
            this.labelCouncil0.Text = "0";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(999, 801);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(151, 224);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 24;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBoxTokens
            // 
            this.pictureBoxTokens.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxTokens.Image")));
            this.pictureBoxTokens.Location = new System.Drawing.Point(1000, 578);
            this.pictureBoxTokens.Name = "pictureBoxTokens";
            this.pictureBoxTokens.Size = new System.Drawing.Size(150, 217);
            this.pictureBoxTokens.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxTokens.TabIndex = 23;
            this.pictureBoxTokens.TabStop = false;
            // 
            // buttonEndTurn
            // 
            this.buttonEndTurn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEndTurn.Location = new System.Drawing.Point(432, 884);
            this.buttonEndTurn.Name = "buttonEndTurn";
            this.buttonEndTurn.Size = new System.Drawing.Size(113, 122);
            this.buttonEndTurn.TabIndex = 22;
            this.buttonEndTurn.Text = "End Turn";
            this.buttonEndTurn.UseVisualStyleBackColor = true;
            this.buttonEndTurn.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBoxLocAv
            // 
            this.pictureBoxLocAv.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLocAv.Image")));
            this.pictureBoxLocAv.Location = new System.Drawing.Point(7, 884);
            this.pictureBoxLocAv.Name = "pictureBoxLocAv";
            this.pictureBoxLocAv.Size = new System.Drawing.Size(419, 122);
            this.pictureBoxLocAv.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLocAv.TabIndex = 21;
            this.pictureBoxLocAv.TabStop = false;
            // 
            // pictureBoxLocStack
            // 
            this.pictureBoxLocStack.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLocStack.Image")));
            this.pictureBoxLocStack.Location = new System.Drawing.Point(551, 884);
            this.pictureBoxLocStack.Name = "pictureBoxLocStack";
            this.pictureBoxLocStack.Size = new System.Drawing.Size(422, 122);
            this.pictureBoxLocStack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLocStack.TabIndex = 20;
            this.pictureBoxLocStack.TabStop = false;
            // 
            // pictureBoxCourt0
            // 
            this.pictureBoxCourt0.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCourt0.Image")));
            this.pictureBoxCourt0.Location = new System.Drawing.Point(838, 635);
            this.pictureBoxCourt0.Name = "pictureBoxCourt0";
            this.pictureBoxCourt0.Size = new System.Drawing.Size(118, 177);
            this.pictureBoxCourt0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCourt0.TabIndex = 19;
            this.pictureBoxCourt0.TabStop = false;
            // 
            // pictureBoxCourt1
            // 
            this.pictureBoxCourt1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCourt1.Image")));
            this.pictureBoxCourt1.Location = new System.Drawing.Point(704, 634);
            this.pictureBoxCourt1.Name = "pictureBoxCourt1";
            this.pictureBoxCourt1.Size = new System.Drawing.Size(117, 179);
            this.pictureBoxCourt1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCourt1.TabIndex = 18;
            this.pictureBoxCourt1.TabStop = false;
            // 
            // pictureBoxCourt2
            // 
            this.pictureBoxCourt2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCourt2.Image")));
            this.pictureBoxCourt2.Location = new System.Drawing.Point(570, 634);
            this.pictureBoxCourt2.Name = "pictureBoxCourt2";
            this.pictureBoxCourt2.Size = new System.Drawing.Size(119, 180);
            this.pictureBoxCourt2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCourt2.TabIndex = 17;
            this.pictureBoxCourt2.TabStop = false;
            // 
            // pictureBoxCourt3
            // 
            this.pictureBoxCourt3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCourt3.Image")));
            this.pictureBoxCourt3.Location = new System.Drawing.Point(434, 627);
            this.pictureBoxCourt3.Name = "pictureBoxCourt3";
            this.pictureBoxCourt3.Size = new System.Drawing.Size(119, 185);
            this.pictureBoxCourt3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCourt3.TabIndex = 16;
            this.pictureBoxCourt3.TabStop = false;
            // 
            // pictureBoxCourt4
            // 
            this.pictureBoxCourt4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCourt4.Image")));
            this.pictureBoxCourt4.Location = new System.Drawing.Point(301, 632);
            this.pictureBoxCourt4.Name = "pictureBoxCourt4";
            this.pictureBoxCourt4.Size = new System.Drawing.Size(116, 181);
            this.pictureBoxCourt4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCourt4.TabIndex = 15;
            this.pictureBoxCourt4.TabStop = false;
            // 
            // pictureBoxCourt5
            // 
            this.pictureBoxCourt5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCourt5.Image")));
            this.pictureBoxCourt5.Location = new System.Drawing.Point(165, 635);
            this.pictureBoxCourt5.Name = "pictureBoxCourt5";
            this.pictureBoxCourt5.Size = new System.Drawing.Size(117, 178);
            this.pictureBoxCourt5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCourt5.TabIndex = 14;
            this.pictureBoxCourt5.TabStop = false;
            // 
            // pictureBoxEx4
            // 
            this.pictureBoxEx4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxEx4.Image")));
            this.pictureBoxEx4.Location = new System.Drawing.Point(884, 54);
            this.pictureBoxEx4.Name = "pictureBoxEx4";
            this.pictureBoxEx4.Size = new System.Drawing.Size(80, 118);
            this.pictureBoxEx4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxEx4.TabIndex = 13;
            this.pictureBoxEx4.TabStop = false;
            // 
            // pictureBoxEx3
            // 
            this.pictureBoxEx3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxEx3.Image")));
            this.pictureBoxEx3.Location = new System.Drawing.Point(712, 56);
            this.pictureBoxEx3.Name = "pictureBoxEx3";
            this.pictureBoxEx3.Size = new System.Drawing.Size(78, 121);
            this.pictureBoxEx3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxEx3.TabIndex = 12;
            this.pictureBoxEx3.TabStop = false;
            // 
            // pictureBoxEx2
            // 
            this.pictureBoxEx2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxEx2.Image")));
            this.pictureBoxEx2.Location = new System.Drawing.Point(541, 56);
            this.pictureBoxEx2.Name = "pictureBoxEx2";
            this.pictureBoxEx2.Size = new System.Drawing.Size(76, 122);
            this.pictureBoxEx2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxEx2.TabIndex = 11;
            this.pictureBoxEx2.TabStop = false;
            // 
            // pictureBoxEx1
            // 
            this.pictureBoxEx1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxEx1.Image")));
            this.pictureBoxEx1.Location = new System.Drawing.Point(368, 56);
            this.pictureBoxEx1.Name = "pictureBoxEx1";
            this.pictureBoxEx1.Size = new System.Drawing.Size(77, 113);
            this.pictureBoxEx1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxEx1.TabIndex = 10;
            this.pictureBoxEx1.TabStop = false;
            // 
            // pictureBoxEx0
            // 
            this.pictureBoxEx0.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxEx0.Image")));
            this.pictureBoxEx0.Location = new System.Drawing.Point(197, 56);
            this.pictureBoxEx0.Name = "pictureBoxEx0";
            this.pictureBoxEx0.Size = new System.Drawing.Size(76, 117);
            this.pictureBoxEx0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxEx0.TabIndex = 9;
            this.pictureBoxEx0.TabStop = false;
            // 
            // pictureBoxCouncil4
            // 
            this.pictureBoxCouncil4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCouncil4.Image")));
            this.pictureBoxCouncil4.Location = new System.Drawing.Point(788, 348);
            this.pictureBoxCouncil4.Name = "pictureBoxCouncil4";
            this.pictureBoxCouncil4.Size = new System.Drawing.Size(144, 173);
            this.pictureBoxCouncil4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCouncil4.TabIndex = 8;
            this.pictureBoxCouncil4.TabStop = false;
            // 
            // pictureBoxCouncil3
            // 
            this.pictureBoxCouncil3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCouncil3.Image")));
            this.pictureBoxCouncil3.Location = new System.Drawing.Point(660, 279);
            this.pictureBoxCouncil3.Name = "pictureBoxCouncil3";
            this.pictureBoxCouncil3.Size = new System.Drawing.Size(120, 173);
            this.pictureBoxCouncil3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCouncil3.TabIndex = 7;
            this.pictureBoxCouncil3.TabStop = false;
            // 
            // pictureBoxCouncil2
            // 
            this.pictureBoxCouncil2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCouncil2.Image")));
            this.pictureBoxCouncil2.Location = new System.Drawing.Point(447, 259);
            this.pictureBoxCouncil2.Name = "pictureBoxCouncil2";
            this.pictureBoxCouncil2.Size = new System.Drawing.Size(112, 181);
            this.pictureBoxCouncil2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCouncil2.TabIndex = 6;
            this.pictureBoxCouncil2.TabStop = false;
            // 
            // pictureBoxCouncil1
            // 
            this.pictureBoxCouncil1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCouncil1.Image")));
            this.pictureBoxCouncil1.Location = new System.Drawing.Point(220, 284);
            this.pictureBoxCouncil1.Name = "pictureBoxCouncil1";
            this.pictureBoxCouncil1.Size = new System.Drawing.Size(130, 170);
            this.pictureBoxCouncil1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCouncil1.TabIndex = 5;
            this.pictureBoxCouncil1.TabStop = false;
            // 
            // pictureBoxCouncil0
            // 
            this.pictureBoxCouncil0.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCouncil0.Image")));
            this.pictureBoxCouncil0.Location = new System.Drawing.Point(59, 360);
            this.pictureBoxCouncil0.Name = "pictureBoxCouncil0";
            this.pictureBoxCouncil0.Size = new System.Drawing.Size(132, 148);
            this.pictureBoxCouncil0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCouncil0.TabIndex = 4;
            this.pictureBoxCouncil0.TabStop = false;
            this.pictureBoxCouncil0.Click += new System.EventHandler(this.pictureBoxCouncil0_Click);
            // 
            // pictureExplore
            // 
            this.pictureExplore.Image = ((System.Drawing.Image)(resources.GetObject("pictureExplore.Image")));
            this.pictureExplore.Location = new System.Drawing.Point(25, 55);
            this.pictureExplore.Name = "pictureExplore";
            this.pictureExplore.Size = new System.Drawing.Size(76, 111);
            this.pictureExplore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureExplore.TabIndex = 3;
            this.pictureExplore.TabStop = false;
            this.pictureExplore.Click += new System.EventHandler(this.pictureExplore_Click);
            // 
            // picturePlotAtCourt
            // 
            this.picturePlotAtCourt.Image = ((System.Drawing.Image)(resources.GetObject("picturePlotAtCourt.Image")));
            this.picturePlotAtCourt.Location = new System.Drawing.Point(23, 624);
            this.picturePlotAtCourt.Name = "picturePlotAtCourt";
            this.picturePlotAtCourt.Size = new System.Drawing.Size(125, 190);
            this.picturePlotAtCourt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picturePlotAtCourt.TabIndex = 2;
            this.picturePlotAtCourt.TabStop = false;
            this.picturePlotAtCourt.Click += new System.EventHandler(this.picturePlotAtCourt_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1005, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(145, 535);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBoxBoard
            // 
            this.pictureBoxBoard.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxBoard.Image")));
            this.pictureBoxBoard.Location = new System.Drawing.Point(7, 37);
            this.pictureBoxBoard.Name = "pictureBoxBoard";
            this.pictureBoxBoard.Size = new System.Drawing.Size(966, 799);
            this.pictureBoxBoard.TabIndex = 0;
            this.pictureBoxBoard.TabStop = false;
            // 
            // groupBoxPlayer
            // 
            this.groupBoxPlayer.Controls.Add(this.groupBoxHand);
            this.groupBoxPlayer.Controls.Add(this.groupBoxTokens);
            this.groupBoxPlayer.Controls.Add(this.groupBoxKeys);
            this.groupBoxPlayer.Controls.Add(this.groupBoxPearls);
            this.groupBoxPlayer.Controls.Add(this.groupBoxAllies);
            this.groupBoxPlayer.Controls.Add(this.groupBoxLocations);
            this.groupBoxPlayer.Controls.Add(this.groupBoxLords);
            this.groupBoxPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxPlayer.Location = new System.Drawing.Point(1190, 13);
            this.groupBoxPlayer.Name = "groupBoxPlayer";
            this.groupBoxPlayer.Size = new System.Drawing.Size(722, 1037);
            this.groupBoxPlayer.TabIndex = 1;
            this.groupBoxPlayer.TabStop = false;
            this.groupBoxPlayer.Text = "Player\'s resources:";
            // 
            // groupBoxTokens
            // 
            this.groupBoxTokens.Controls.Add(this.listBoxTokens);
            this.groupBoxTokens.Location = new System.Drawing.Point(544, 358);
            this.groupBoxTokens.Name = "groupBoxTokens";
            this.groupBoxTokens.Size = new System.Drawing.Size(172, 180);
            this.groupBoxTokens.TabIndex = 4;
            this.groupBoxTokens.TabStop = false;
            this.groupBoxTokens.Text = "Monster Tokens:";
            // 
            // groupBoxKeys
            // 
            this.groupBoxKeys.Controls.Add(this.labelKeys);
            this.groupBoxKeys.Location = new System.Drawing.Point(544, 200);
            this.groupBoxKeys.Name = "groupBoxKeys";
            this.groupBoxKeys.Size = new System.Drawing.Size(172, 136);
            this.groupBoxKeys.TabIndex = 4;
            this.groupBoxKeys.TabStop = false;
            this.groupBoxKeys.Text = "Keys:";
            // 
            // labelKeys
            // 
            this.labelKeys.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelKeys.Location = new System.Drawing.Point(6, 22);
            this.labelKeys.Name = "labelKeys";
            this.labelKeys.Size = new System.Drawing.Size(160, 112);
            this.labelKeys.TabIndex = 1;
            this.labelKeys.Text = "0";
            this.labelKeys.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxPearls
            // 
            this.groupBoxPearls.Controls.Add(this.labelPearls);
            this.groupBoxPearls.Location = new System.Drawing.Point(544, 37);
            this.groupBoxPearls.Name = "groupBoxPearls";
            this.groupBoxPearls.Size = new System.Drawing.Size(172, 157);
            this.groupBoxPearls.TabIndex = 3;
            this.groupBoxPearls.TabStop = false;
            this.groupBoxPearls.Text = "Pearls:";
            // 
            // labelPearls
            // 
            this.labelPearls.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPearls.Location = new System.Drawing.Point(6, 27);
            this.labelPearls.Name = "labelPearls";
            this.labelPearls.Size = new System.Drawing.Size(160, 117);
            this.labelPearls.TabIndex = 0;
            this.labelPearls.Text = "0";
            this.labelPearls.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxAllies
            // 
            this.groupBoxAllies.Controls.Add(this.pictureBoxAlly7);
            this.groupBoxAllies.Controls.Add(this.pictureBoxAlly6);
            this.groupBoxAllies.Controls.Add(this.pictureBoxAlly5);
            this.groupBoxAllies.Controls.Add(this.pictureBoxAlly4);
            this.groupBoxAllies.Controls.Add(this.pictureBoxAlly3);
            this.groupBoxAllies.Controls.Add(this.pictureBoxAlly2);
            this.groupBoxAllies.Controls.Add(this.pictureBoxAlly1);
            this.groupBoxAllies.Controls.Add(this.pictureBoxAlly0);
            this.groupBoxAllies.Location = new System.Drawing.Point(6, 852);
            this.groupBoxAllies.Name = "groupBoxAllies";
            this.groupBoxAllies.Size = new System.Drawing.Size(710, 179);
            this.groupBoxAllies.TabIndex = 2;
            this.groupBoxAllies.TabStop = false;
            this.groupBoxAllies.Text = "Affiliated Allies:";
            // 
            // pictureBoxAlly7
            // 
            this.pictureBoxAlly7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAlly7.Image")));
            this.pictureBoxAlly7.Location = new System.Drawing.Point(603, 36);
            this.pictureBoxAlly7.Name = "pictureBoxAlly7";
            this.pictureBoxAlly7.Size = new System.Drawing.Size(76, 111);
            this.pictureBoxAlly7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxAlly7.TabIndex = 11;
            this.pictureBoxAlly7.TabStop = false;
            // 
            // pictureBoxAlly6
            // 
            this.pictureBoxAlly6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAlly6.Image")));
            this.pictureBoxAlly6.Location = new System.Drawing.Point(521, 36);
            this.pictureBoxAlly6.Name = "pictureBoxAlly6";
            this.pictureBoxAlly6.Size = new System.Drawing.Size(76, 111);
            this.pictureBoxAlly6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxAlly6.TabIndex = 10;
            this.pictureBoxAlly6.TabStop = false;
            // 
            // pictureBoxAlly5
            // 
            this.pictureBoxAlly5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAlly5.Image")));
            this.pictureBoxAlly5.Location = new System.Drawing.Point(439, 36);
            this.pictureBoxAlly5.Name = "pictureBoxAlly5";
            this.pictureBoxAlly5.Size = new System.Drawing.Size(76, 111);
            this.pictureBoxAlly5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxAlly5.TabIndex = 9;
            this.pictureBoxAlly5.TabStop = false;
            // 
            // pictureBoxAlly4
            // 
            this.pictureBoxAlly4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAlly4.Image")));
            this.pictureBoxAlly4.Location = new System.Drawing.Point(357, 36);
            this.pictureBoxAlly4.Name = "pictureBoxAlly4";
            this.pictureBoxAlly4.Size = new System.Drawing.Size(76, 111);
            this.pictureBoxAlly4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxAlly4.TabIndex = 8;
            this.pictureBoxAlly4.TabStop = false;
            // 
            // pictureBoxAlly3
            // 
            this.pictureBoxAlly3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAlly3.Image")));
            this.pictureBoxAlly3.Location = new System.Drawing.Point(275, 36);
            this.pictureBoxAlly3.Name = "pictureBoxAlly3";
            this.pictureBoxAlly3.Size = new System.Drawing.Size(76, 111);
            this.pictureBoxAlly3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxAlly3.TabIndex = 7;
            this.pictureBoxAlly3.TabStop = false;
            // 
            // pictureBoxAlly2
            // 
            this.pictureBoxAlly2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAlly2.Image")));
            this.pictureBoxAlly2.Location = new System.Drawing.Point(193, 36);
            this.pictureBoxAlly2.Name = "pictureBoxAlly2";
            this.pictureBoxAlly2.Size = new System.Drawing.Size(76, 111);
            this.pictureBoxAlly2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxAlly2.TabIndex = 6;
            this.pictureBoxAlly2.TabStop = false;
            // 
            // pictureBoxAlly1
            // 
            this.pictureBoxAlly1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAlly1.Image")));
            this.pictureBoxAlly1.Location = new System.Drawing.Point(111, 36);
            this.pictureBoxAlly1.Name = "pictureBoxAlly1";
            this.pictureBoxAlly1.Size = new System.Drawing.Size(76, 111);
            this.pictureBoxAlly1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxAlly1.TabIndex = 5;
            this.pictureBoxAlly1.TabStop = false;
            // 
            // pictureBoxAlly0
            // 
            this.pictureBoxAlly0.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAlly0.Image")));
            this.pictureBoxAlly0.Location = new System.Drawing.Point(29, 36);
            this.pictureBoxAlly0.Name = "pictureBoxAlly0";
            this.pictureBoxAlly0.Size = new System.Drawing.Size(76, 111);
            this.pictureBoxAlly0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxAlly0.TabIndex = 4;
            this.pictureBoxAlly0.TabStop = false;
            // 
            // groupBoxLocations
            // 
            this.groupBoxLocations.Controls.Add(this.listBoxLocations);
            this.groupBoxLocations.Controls.Add(this.pictureBoxLoc0);
            this.groupBoxLocations.Location = new System.Drawing.Point(6, 544);
            this.groupBoxLocations.Name = "groupBoxLocations";
            this.groupBoxLocations.Size = new System.Drawing.Size(443, 302);
            this.groupBoxLocations.TabIndex = 1;
            this.groupBoxLocations.TabStop = false;
            this.groupBoxLocations.Text = "Locations";
            // 
            // pictureBoxLoc0
            // 
            this.pictureBoxLoc0.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLoc0.Image")));
            this.pictureBoxLoc0.Location = new System.Drawing.Point(10, 30);
            this.pictureBoxLoc0.Name = "pictureBoxLoc0";
            this.pictureBoxLoc0.Size = new System.Drawing.Size(422, 122);
            this.pictureBoxLoc0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLoc0.TabIndex = 21;
            this.pictureBoxLoc0.TabStop = false;
            // 
            // groupBoxLords
            // 
            this.groupBoxLords.Controls.Add(this.labelLord7);
            this.groupBoxLords.Controls.Add(this.labelLord6);
            this.groupBoxLords.Controls.Add(this.labelLord5);
            this.groupBoxLords.Controls.Add(this.labelLord4);
            this.groupBoxLords.Controls.Add(this.labelLord3);
            this.groupBoxLords.Controls.Add(this.labelLord2);
            this.groupBoxLords.Controls.Add(this.labelLord1);
            this.groupBoxLords.Controls.Add(this.labelLord0);
            this.groupBoxLords.Controls.Add(this.pictureBoxLord7);
            this.groupBoxLords.Controls.Add(this.pictureBoxLord6);
            this.groupBoxLords.Controls.Add(this.pictureBoxLord5);
            this.groupBoxLords.Controls.Add(this.pictureBoxLord4);
            this.groupBoxLords.Controls.Add(this.pictureBoxLord3);
            this.groupBoxLords.Controls.Add(this.pictureBoxLord2);
            this.groupBoxLords.Controls.Add(this.pictureBoxLord1);
            this.groupBoxLords.Controls.Add(this.pictureBoxLord0);
            this.groupBoxLords.Location = new System.Drawing.Point(6, 25);
            this.groupBoxLords.Name = "groupBoxLords";
            this.groupBoxLords.Size = new System.Drawing.Size(532, 513);
            this.groupBoxLords.TabIndex = 0;
            this.groupBoxLords.TabStop = false;
            this.groupBoxLords.Text = "Lords:";
            // 
            // labelLord7
            // 
            this.labelLord7.Location = new System.Drawing.Point(399, 465);
            this.labelLord7.Name = "labelLord7";
            this.labelLord7.Size = new System.Drawing.Size(125, 31);
            this.labelLord7.TabIndex = 18;
            this.labelLord7.Text = "N/A";
            this.labelLord7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLord6
            // 
            this.labelLord6.Location = new System.Drawing.Point(268, 465);
            this.labelLord6.Name = "labelLord6";
            this.labelLord6.Size = new System.Drawing.Size(125, 31);
            this.labelLord6.TabIndex = 17;
            this.labelLord6.Text = "N/A";
            this.labelLord6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLord5
            // 
            this.labelLord5.Location = new System.Drawing.Point(137, 466);
            this.labelLord5.Name = "labelLord5";
            this.labelLord5.Size = new System.Drawing.Size(125, 31);
            this.labelLord5.TabIndex = 16;
            this.labelLord5.Text = "N/A";
            this.labelLord5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLord4
            // 
            this.labelLord4.Location = new System.Drawing.Point(6, 466);
            this.labelLord4.Name = "labelLord4";
            this.labelLord4.Size = new System.Drawing.Size(125, 31);
            this.labelLord4.TabIndex = 15;
            this.labelLord4.Text = "N/A";
            this.labelLord4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLord3
            // 
            this.labelLord3.Location = new System.Drawing.Point(399, 223);
            this.labelLord3.Name = "labelLord3";
            this.labelLord3.Size = new System.Drawing.Size(125, 31);
            this.labelLord3.TabIndex = 14;
            this.labelLord3.Text = "N/A";
            this.labelLord3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLord2
            // 
            this.labelLord2.Location = new System.Drawing.Point(268, 223);
            this.labelLord2.Name = "labelLord2";
            this.labelLord2.Size = new System.Drawing.Size(125, 31);
            this.labelLord2.TabIndex = 13;
            this.labelLord2.Text = "N/A";
            this.labelLord2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLord1
            // 
            this.labelLord1.Location = new System.Drawing.Point(137, 224);
            this.labelLord1.Name = "labelLord1";
            this.labelLord1.Size = new System.Drawing.Size(125, 31);
            this.labelLord1.TabIndex = 12;
            this.labelLord1.Text = "N/A";
            this.labelLord1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLord0
            // 
            this.labelLord0.Location = new System.Drawing.Point(6, 224);
            this.labelLord0.Name = "labelLord0";
            this.labelLord0.Size = new System.Drawing.Size(125, 31);
            this.labelLord0.TabIndex = 11;
            this.labelLord0.Text = "N/A";
            this.labelLord0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBoxLord7
            // 
            this.pictureBoxLord7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLord7.Image")));
            this.pictureBoxLord7.Location = new System.Drawing.Point(399, 273);
            this.pictureBoxLord7.Name = "pictureBoxLord7";
            this.pictureBoxLord7.Size = new System.Drawing.Size(125, 190);
            this.pictureBoxLord7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLord7.TabIndex = 10;
            this.pictureBoxLord7.TabStop = false;
            // 
            // pictureBoxLord6
            // 
            this.pictureBoxLord6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLord6.Image")));
            this.pictureBoxLord6.Location = new System.Drawing.Point(268, 273);
            this.pictureBoxLord6.Name = "pictureBoxLord6";
            this.pictureBoxLord6.Size = new System.Drawing.Size(125, 190);
            this.pictureBoxLord6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLord6.TabIndex = 9;
            this.pictureBoxLord6.TabStop = false;
            // 
            // pictureBoxLord5
            // 
            this.pictureBoxLord5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLord5.Image")));
            this.pictureBoxLord5.Location = new System.Drawing.Point(137, 273);
            this.pictureBoxLord5.Name = "pictureBoxLord5";
            this.pictureBoxLord5.Size = new System.Drawing.Size(125, 190);
            this.pictureBoxLord5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLord5.TabIndex = 8;
            this.pictureBoxLord5.TabStop = false;
            // 
            // pictureBoxLord4
            // 
            this.pictureBoxLord4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLord4.Image")));
            this.pictureBoxLord4.Location = new System.Drawing.Point(6, 273);
            this.pictureBoxLord4.Name = "pictureBoxLord4";
            this.pictureBoxLord4.Size = new System.Drawing.Size(125, 190);
            this.pictureBoxLord4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLord4.TabIndex = 7;
            this.pictureBoxLord4.TabStop = false;
            // 
            // pictureBoxLord3
            // 
            this.pictureBoxLord3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLord3.Image")));
            this.pictureBoxLord3.Location = new System.Drawing.Point(399, 31);
            this.pictureBoxLord3.Name = "pictureBoxLord3";
            this.pictureBoxLord3.Size = new System.Drawing.Size(125, 190);
            this.pictureBoxLord3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLord3.TabIndex = 6;
            this.pictureBoxLord3.TabStop = false;
            // 
            // pictureBoxLord2
            // 
            this.pictureBoxLord2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLord2.Image")));
            this.pictureBoxLord2.Location = new System.Drawing.Point(268, 31);
            this.pictureBoxLord2.Name = "pictureBoxLord2";
            this.pictureBoxLord2.Size = new System.Drawing.Size(125, 190);
            this.pictureBoxLord2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLord2.TabIndex = 5;
            this.pictureBoxLord2.TabStop = false;
            // 
            // pictureBoxLord1
            // 
            this.pictureBoxLord1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLord1.Image")));
            this.pictureBoxLord1.Location = new System.Drawing.Point(137, 31);
            this.pictureBoxLord1.Name = "pictureBoxLord1";
            this.pictureBoxLord1.Size = new System.Drawing.Size(125, 190);
            this.pictureBoxLord1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLord1.TabIndex = 4;
            this.pictureBoxLord1.TabStop = false;
            // 
            // pictureBoxLord0
            // 
            this.pictureBoxLord0.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLord0.Image")));
            this.pictureBoxLord0.Location = new System.Drawing.Point(6, 31);
            this.pictureBoxLord0.Name = "pictureBoxLord0";
            this.pictureBoxLord0.Size = new System.Drawing.Size(125, 190);
            this.pictureBoxLord0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLord0.TabIndex = 3;
            this.pictureBoxLord0.TabStop = false;
            // 
            // listBoxTokens
            // 
            this.listBoxTokens.FormattingEnabled = true;
            this.listBoxTokens.ItemHeight = 20;
            this.listBoxTokens.Location = new System.Drawing.Point(6, 25);
            this.listBoxTokens.Name = "listBoxTokens";
            this.listBoxTokens.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBoxTokens.Size = new System.Drawing.Size(160, 144);
            this.listBoxTokens.TabIndex = 1;
            // 
            // groupBoxHand
            // 
            this.groupBoxHand.Controls.Add(this.listBoxHand);
            this.groupBoxHand.Location = new System.Drawing.Point(455, 544);
            this.groupBoxHand.Name = "groupBoxHand";
            this.groupBoxHand.Size = new System.Drawing.Size(261, 302);
            this.groupBoxHand.TabIndex = 5;
            this.groupBoxHand.TabStop = false;
            this.groupBoxHand.Text = "Hand";
            // 
            // listBoxLocations
            // 
            this.listBoxLocations.FormattingEnabled = true;
            this.listBoxLocations.ItemHeight = 20;
            this.listBoxLocations.Location = new System.Drawing.Point(11, 165);
            this.listBoxLocations.Name = "listBoxLocations";
            this.listBoxLocations.Size = new System.Drawing.Size(421, 124);
            this.listBoxLocations.TabIndex = 22;
            // 
            // listBoxHand
            // 
            this.listBoxHand.FormattingEnabled = true;
            this.listBoxHand.ItemHeight = 20;
            this.listBoxHand.Location = new System.Drawing.Point(6, 25);
            this.listBoxHand.Name = "listBoxHand";
            this.listBoxHand.Size = new System.Drawing.Size(249, 264);
            this.listBoxHand.TabIndex = 23;
            // 
            // FormGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 1062);
            this.Controls.Add(this.groupBoxPlayer);
            this.Controls.Add(this.groupBoxBoard);
            this.Name = "FormGame";
            this.Text = "Abyss";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBoxBoard.ResumeLayout(false);
            this.groupBoxBoard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTokens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLocAv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLocStack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCourt5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEx4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEx3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEx2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEx1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEx0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCouncil4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCouncil3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCouncil2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCouncil1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCouncil0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureExplore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturePlotAtCourt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBoard)).EndInit();
            this.groupBoxPlayer.ResumeLayout(false);
            this.groupBoxTokens.ResumeLayout(false);
            this.groupBoxKeys.ResumeLayout(false);
            this.groupBoxPearls.ResumeLayout(false);
            this.groupBoxAllies.ResumeLayout(false);
            this.groupBoxAllies.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlly0)).EndInit();
            this.groupBoxLocations.ResumeLayout(false);
            this.groupBoxLocations.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoc0)).EndInit();
            this.groupBoxLords.ResumeLayout(false);
            this.groupBoxLords.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLord0)).EndInit();
            this.groupBoxHand.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxBoard;
        private System.Windows.Forms.PictureBox pictureBoxBoard;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBoxCouncil4;
        private System.Windows.Forms.PictureBox pictureBoxCouncil3;
        private System.Windows.Forms.PictureBox pictureBoxCouncil2;
        private System.Windows.Forms.PictureBox pictureBoxCouncil1;
        private System.Windows.Forms.PictureBox pictureBoxCouncil0;
        private System.Windows.Forms.PictureBox pictureExplore;
        private System.Windows.Forms.PictureBox picturePlotAtCourt;
        private System.Windows.Forms.PictureBox pictureBoxCourt0;
        private System.Windows.Forms.PictureBox pictureBoxCourt1;
        private System.Windows.Forms.PictureBox pictureBoxCourt2;
        private System.Windows.Forms.PictureBox pictureBoxCourt3;
        private System.Windows.Forms.PictureBox pictureBoxCourt4;
        private System.Windows.Forms.PictureBox pictureBoxCourt5;
        private System.Windows.Forms.PictureBox pictureBoxEx4;
        private System.Windows.Forms.PictureBox pictureBoxEx3;
        private System.Windows.Forms.PictureBox pictureBoxEx2;
        private System.Windows.Forms.PictureBox pictureBoxEx1;
        private System.Windows.Forms.PictureBox pictureBoxEx0;
        private System.Windows.Forms.PictureBox pictureBoxLocAv;
        private System.Windows.Forms.PictureBox pictureBoxLocStack;
        private System.Windows.Forms.Button buttonEndTurn;
        private System.Windows.Forms.GroupBox groupBoxPlayer;
        private System.Windows.Forms.GroupBox groupBoxLords;
        private System.Windows.Forms.PictureBox pictureBoxLord7;
        private System.Windows.Forms.PictureBox pictureBoxLord6;
        private System.Windows.Forms.PictureBox pictureBoxLord5;
        private System.Windows.Forms.PictureBox pictureBoxLord4;
        private System.Windows.Forms.PictureBox pictureBoxLord3;
        private System.Windows.Forms.PictureBox pictureBoxLord2;
        private System.Windows.Forms.PictureBox pictureBoxLord1;
        private System.Windows.Forms.PictureBox pictureBoxLord0;
        private System.Windows.Forms.Label labelLord0;
        private System.Windows.Forms.Label labelLord7;
        private System.Windows.Forms.Label labelLord6;
        private System.Windows.Forms.Label labelLord5;
        private System.Windows.Forms.Label labelLord4;
        private System.Windows.Forms.Label labelLord3;
        private System.Windows.Forms.Label labelLord2;
        private System.Windows.Forms.Label labelLord1;
        private System.Windows.Forms.GroupBox groupBoxLocations;
        private System.Windows.Forms.PictureBox pictureBoxLoc0;
        private System.Windows.Forms.GroupBox groupBoxTokens;
        private System.Windows.Forms.GroupBox groupBoxKeys;
        private System.Windows.Forms.Label labelKeys;
        private System.Windows.Forms.GroupBox groupBoxPearls;
        private System.Windows.Forms.Label labelPearls;
        private System.Windows.Forms.GroupBox groupBoxAllies;
        private System.Windows.Forms.PictureBox pictureBoxAlly7;
        private System.Windows.Forms.PictureBox pictureBoxAlly6;
        private System.Windows.Forms.PictureBox pictureBoxAlly5;
        private System.Windows.Forms.PictureBox pictureBoxAlly4;
        private System.Windows.Forms.PictureBox pictureBoxAlly3;
        private System.Windows.Forms.PictureBox pictureBoxAlly2;
        private System.Windows.Forms.PictureBox pictureBoxAlly1;
        private System.Windows.Forms.PictureBox pictureBoxAlly0;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBoxTokens;
        private System.Windows.Forms.Label labelCouncil4;
        private System.Windows.Forms.Label labelCouncil3;
        private System.Windows.Forms.Label labelCouncil2;
        private System.Windows.Forms.Label labelCouncil1;
        private System.Windows.Forms.Label labelCouncil0;
        private System.Windows.Forms.ListBox listBoxTokens;
        private System.Windows.Forms.GroupBox groupBoxHand;
        private System.Windows.Forms.ListBox listBoxHand;
        private System.Windows.Forms.ListBox listBoxLocations;
    }
}