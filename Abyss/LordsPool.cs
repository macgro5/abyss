﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abyss
{
    public enum LordsPool
    {
        Hermit,
        Elder,
        Sage,

        Shaman,
        Invoker,
        Illusionist,
        Oracle,
        MasterOfMagic,
        Apprentice,
        Alchemist,

        Comaner,
        Hunter,
        Seeker,
        Assassin,
        Tamer,
        Recruiter,
        Jailer,

        Diplomat,
        Traitor,
        Treasurer,
        Corruptor,
        Scheemer,
        Opportunist,

        Keeper,    
        Miller,                
        Reaper,        
        Shepherd,
        Aquaculturalist,
        Landowner,
                
        Shopkeeper,      
        Landlord,        
        ShipMaster,        
        Trader,
        Slaver,        
        Peddler        
    }
}
