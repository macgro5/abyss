﻿namespace Abyss
{
    partial class FormAorBQuestion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelQuestion = new System.Windows.Forms.Label();
            this.buttonTrue = new System.Windows.Forms.Button();
            this.buttonFalse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelQuestion
            // 
            this.labelQuestion.AutoSize = true;
            this.labelQuestion.Location = new System.Drawing.Point(12, 47);
            this.labelQuestion.Name = "labelQuestion";
            this.labelQuestion.Size = new System.Drawing.Size(73, 13);
            this.labelQuestion.TabIndex = 0;
            this.labelQuestion.Text = "Question Text";
            // 
            // buttonTrue
            // 
            this.buttonTrue.Location = new System.Drawing.Point(75, 94);
            this.buttonTrue.Name = "buttonTrue";
            this.buttonTrue.Size = new System.Drawing.Size(98, 23);
            this.buttonTrue.TabIndex = 1;
            this.buttonTrue.Text = "Pearl";
            this.buttonTrue.UseVisualStyleBackColor = true;
            this.buttonTrue.Click += new System.EventHandler(this.buttonPearl_Click);
            // 
            // buttonFalse
            // 
            this.buttonFalse.Location = new System.Drawing.Point(179, 94);
            this.buttonFalse.Name = "buttonFalse";
            this.buttonFalse.Size = new System.Drawing.Size(98, 23);
            this.buttonFalse.TabIndex = 2;
            this.buttonFalse.Text = "Monster Token";
            this.buttonFalse.UseVisualStyleBackColor = true;
            this.buttonFalse.Click += new System.EventHandler(this.buttonToken_Click);
            // 
            // FormAorBQuestion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 136);
            this.Controls.Add(this.buttonFalse);
            this.Controls.Add(this.buttonTrue);
            this.Controls.Add(this.labelQuestion);
            this.Name = "FormAorBQuestion";
            this.Text = "FormAorBQuestion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label labelQuestion;
        public System.Windows.Forms.Button buttonTrue;
        public System.Windows.Forms.Button buttonFalse;
    }
}