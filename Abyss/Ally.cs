﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abyss
{
    public class Ally
    {
        public readonly RaceOption Race;
        public readonly int Points;
        public readonly string Name;

        public Ally(RaceOption race,int points)
        {
            Race = race;
            Points = points;
            Name = Race.ToString() + Points.ToString();
        }
        public override string ToString()
        {
            return Points + "\t" + Race;
        }
    }
}
