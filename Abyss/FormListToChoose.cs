﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Abyss
{
    public partial class FormListToChoose : Form
    {
        public Object ReturnValue { get; set; }
        
        public FormListToChoose()
        {
            InitializeComponent();
        }


        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            switch (listBox1.SelectionMode)
            {
                case SelectionMode.One:
                    ReturnValue = listBox1.SelectedItem;
                    DialogResult = DialogResult.OK;
                    break;
                case SelectionMode.MultiSimple:
                    ReturnValue = listBox1.SelectedItems.Cast<Lord>().ToList<Lord>();
                    DialogResult = DialogResult.OK;
                    break;
                default:
                    throw new Exception();
            }
        }
    }
}
