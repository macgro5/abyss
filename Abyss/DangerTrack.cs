﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abyss;
using System.Windows.Forms;

namespace Abyss
{
    public enum DangerLevel
    {
        lev1,
        lev2,
        lev3,
        lev4,
        lev5,
        lev6
    }

    public class DangerTrack
    {
        public List<int> MonsterTokensPool;
        private DangerLevel dangerLevel;
        public const int TotalOf2pointTokens = 10;
        public const int TotalOf3pointTokens = 7;
        public const int TotalOf4pointTokens = 3;
        private IQuestions _questions;
              
        public DangerTrack()
        {
            MonsterTokensPool = new List<int>();
            for (int i = 0; i < TotalOf2pointTokens; i++)
            {
                MonsterTokensPool.Add(2);
            }
            for(int i = 0; i < TotalOf3pointTokens; i++)
            {
                MonsterTokensPool.Add(3);
            }
            for(int i =0; i < TotalOf4pointTokens; i++)
            {
                MonsterTokensPool.Add(4);
            }

            dangerLevel = DangerLevel.lev1;
            _questions = new Questions();
        }

        private void ChosseReward(Player player, int numberOfLoops)//Done
        {
            for (int i = 0; i < numberOfLoops; i++)
            {
                if (_questions.PearlOrTokenQuestion(player.PlayerName))
                    player.Pearls++;
                else
                {
                    var random = new Random();
                    int id = random.Next(0, MonsterTokensPool.Count-1);
                    player.PlayersMonsterTokens.Add(MonsterTokensPool[id]);
                    MonsterTokensPool.RemoveAt(id);
                }
            }
        }

        public void FightMonster(Player player)//Done
        {
            switch (dangerLevel)
            {
                case DangerLevel.lev1:
                    ChosseReward(player, 1);
                    dangerLevel = DangerLevel.lev1;
                    break;
                case DangerLevel.lev2:
                    ChosseReward(player, 2);
                    dangerLevel = DangerLevel.lev1;
                    break;
                case DangerLevel.lev3:
                    player.PlayersKeys++;
                    dangerLevel = DangerLevel.lev1;
                    break;
                case DangerLevel.lev4:
                    player.PlayersKeys++;
                    ChosseReward(player, 1);
                    dangerLevel = DangerLevel.lev1;
                    break;
                case DangerLevel.lev5:
                    player.PlayersKeys++;
                    ChosseReward(player, 2);
                    dangerLevel = DangerLevel.lev1;
                    break;
                case DangerLevel.lev6:
                    player.PlayersKeys += 2;
                    dangerLevel = DangerLevel.lev1;
                    break;
            }
        }

        public void ProgressDanger()//Done
        {
            if ((int)dangerLevel < 5)
            {
                switch (dangerLevel)
                {
                    case DangerLevel.lev1:
                        dangerLevel = DangerLevel.lev2;
                        break;
                    case DangerLevel.lev2:
                        dangerLevel = DangerLevel.lev3;
                        break;
                    case DangerLevel.lev3:
                        dangerLevel = DangerLevel.lev4;
                        break;
                    case DangerLevel.lev4:
                        dangerLevel = DangerLevel.lev5;
                        break;
                    case DangerLevel.lev5:
                        dangerLevel = DangerLevel.lev6;
                        break;
                }
            }
        }
    }
}
