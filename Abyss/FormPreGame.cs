﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Abyss
{
    public partial class FormPreGame : Form
    {
        public FormPreGame()
        {
            InitializeComponent();
        }

        private List<Player> CreatePlayers(int numberOfPlayers)
        {
            var Players = new List<Player>();
            for (int i = 1; i<=numberOfPlayers; i++)
            {
                var accName = 'p' + i.ToString();
                var textBox = Controls.OfType<TextBox>()
                                      .FirstOrDefault(a => a.AccessibleName==accName);
                var player = new Player(textBox.Text,i);
                Players.Add(player);
            }
            return Players;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            Player1name.Enabled = true;
            Player2name.Enabled = true;
            Player3name.Enabled = false;
            Player4name.Enabled = false;

            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = false;
            textBox4.Enabled = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            Player1name.Enabled = true;
            Player2name.Enabled = true;
            Player3name.Enabled = true;
            Player4name.Enabled = false;

            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            textBox4.Enabled = false;
        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            Player1name.Enabled = true;
            Player2name.Enabled = true;
            Player3name.Enabled = true;
            Player4name.Enabled = true;

            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            textBox4.Enabled = true;
        }
        private void startButton_Click(object sender, EventArgs e)
        {
            List<Player> Players = new List<Player>();
            var checkedButton = Controls.OfType<RadioButton>()
                                      .FirstOrDefault(r => r.Checked);
            switch (checkedButton.AccessibleName)
            {
                case "2p":
                    Players.AddRange(CreatePlayers(2));
                    break;
                case "3p":
                    Players.AddRange(CreatePlayers(3));
                    break;
                case "4p":
                    Players.AddRange(CreatePlayers(4));
                    break;
            }
            var formGame = new FormGame(new Game(Players,new DecksGenerator()));
            foreach (Player player in Players)
                player.Game = formGame.Game;
            formGame.Game.ActivePlayer.ExplorationTrackChange += formGame.OnExplorationTrackChange;
            formGame.Game.ActivePlayer.CourtChange += formGame.OnCourtChange;
            formGame.Game.ActivePlayer.CouncilChange += formGame.OnCouncilChange;
            formGame.Game.ActivePlayer.PlayersLordsChange += formGame.OnPlayersLordChange;
            formGame.Game.ActivePlayer.PlayersAffiliatedAlliesChange += formGame.OnPlayersAffiliatedAlliesChange;
            formGame.Game.ActivePlayer.PlayersPearlsChange += formGame.OnPlayersPearlsChange;
            formGame.Game.ActivePlayer.PlayersKeysChange += formGame.OnPlayersKeysChange;
            formGame.Game.ActivePlayer.PlayersTokensChange += formGame.OnPlayersTokensChange;
            formGame.Game.ActivePlayer.PlayersLocationsChange += formGame.OnPlayersLocationsChange;
            formGame.Game.ActivePlayer.PlayersHandChange += formGame.OnPlayersHandChange;
            formGame.Show();
            Hide();
        }


    }
}

        
    
