﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Abyss
{
    

    public class Lord
    {
        public LordsPool Id { get; private set; }
        public RaceOption Guild { get; private set; }
        public string Name { get; private set; }
        public int Points { get; private set; }
        public int Keys { get; private set; }
        public int PointCost { get; private set; }
        public int DifferentRaces { get; private set; }
        public bool IsControlingLocation { get; set; }
        public string AbilityDescribtion { get; private set; }

        public Lord(LordsPool id, RaceOption guild, string name, int points, int keys, int pointCost, int differentRaces)
        {
            Id=id;
            Guild = guild;
            Name = name;
            Points = points;
            Keys = keys;
            PointCost = pointCost;
            DifferentRaces = differentRaces;               
        }

        public void Ability()
        {
            ILordsAbilities skill=new LordsAbilities();
            skill.Ability(Id);
        }
        public override string ToString()
        {
            return Name+"\t"+Keys + " keys\t" + AbilityDescribtion;
        }
    }
}
