﻿namespace Abyss
{
    partial class FormPreGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.Player1name = new System.Windows.Forms.Label();
            this.Player2name = new System.Windows.Forms.Label();
            this.Player3name = new System.Windows.Forms.Label();
            this.Player4name = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.startButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.AccessibleName = "p1";
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(146, 56);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 0;
            // 
            // textBox2
            // 
            this.textBox2.AccessibleName = "p2";
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(146, 84);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.AccessibleName = "p3";
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(146, 113);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 2;
            // 
            // textBox4
            // 
            this.textBox4.AccessibleName = "p4";
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(146, 139);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 3;
            // 
            // Player1name
            // 
            this.Player1name.AutoSize = true;
            this.Player1name.Location = new System.Drawing.Point(51, 59);
            this.Player1name.Name = "Player1name";
            this.Player1name.Size = new System.Drawing.Size(89, 13);
            this.Player1name.TabIndex = 4;
            this.Player1name.Text = "Player one name:";
            this.Player1name.Click += new System.EventHandler(this.label1_Click);
            // 
            // Player2name
            // 
            this.Player2name.AutoSize = true;
            this.Player2name.Location = new System.Drawing.Point(51, 89);
            this.Player2name.Name = "Player2name";
            this.Player2name.Size = new System.Drawing.Size(88, 13);
            this.Player2name.TabIndex = 5;
            this.Player2name.Text = "Player two name:";
            // 
            // Player3name
            // 
            this.Player3name.AutoSize = true;
            this.Player3name.Location = new System.Drawing.Point(51, 116);
            this.Player3name.Name = "Player3name";
            this.Player3name.Size = new System.Drawing.Size(92, 13);
            this.Player3name.TabIndex = 6;
            this.Player3name.Text = "Player three name";
            // 
            // Player4name
            // 
            this.Player4name.AutoSize = true;
            this.Player4name.Location = new System.Drawing.Point(51, 143);
            this.Player4name.Name = "Player4name";
            this.Player4name.Size = new System.Drawing.Size(89, 13);
            this.Player4name.TabIndex = 7;
            this.Player4name.Text = "Player four name:";
            // 
            // radioButton1
            // 
            this.radioButton1.AccessibleName = "2p";
            this.radioButton1.AccessibleRole = System.Windows.Forms.AccessibleRole.RadioButton;
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(54, 24);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(67, 17);
            this.radioButton1.TabIndex = 8;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "2 players";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AccessibleName = "3p";
            this.radioButton2.AccessibleRole = System.Windows.Forms.AccessibleRole.RadioButton;
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(145, 24);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(67, 17);
            this.radioButton2.TabIndex = 9;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "3 players";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AccessibleName = "4p";
            this.radioButton3.AccessibleRole = System.Windows.Forms.AccessibleRole.RadioButton;
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(236, 24);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(67, 17);
            this.radioButton3.TabIndex = 10;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "4 players";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(54, 179);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(249, 32);
            this.startButton.TabIndex = 11;
            this.startButton.Text = "Start Game";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // FormPreGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 234);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.Player4name);
            this.Controls.Add(this.Player3name);
            this.Controls.Add(this.Player2name);
            this.Controls.Add(this.Player1name);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Name = "FormPreGame";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label Player1name;
        private System.Windows.Forms.Label Player2name;
        private System.Windows.Forms.Label Player3name;
        private System.Windows.Forms.Label Player4name;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Button startButton;
    }
}

