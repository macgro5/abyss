﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abyss
{
    interface IQuestions
    {
        bool YesNoQuestion(string question, string windowName);
        bool PearlOrTokenQuestion(string playerName);
        bool AvailableOrStackLocations(string playerName);
        Ally AllyToAffiliate(List<Ally> allies);
        Location LocationToChose(List<Location> listOfLocations);
        List<Lord> ChoseLordsForLocation(List<Lord> lordsToChose,int keysRequired);
        int HowManyLocsFromStack(string playersName);
    }
}
