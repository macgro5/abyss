﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abyss;
using System.Windows.Forms;

namespace Abyss
{
    public class Player
    {
        public Game Game;
        public readonly int PlayerSequence;
        public readonly string PlayerName;
        public List<Lord> PlayersLords { get; private set; }
        public List<Ally> AffiliatedAllies { get; private set; }
        public List<Location> PlayersLocations { get; private set; }
        public List<Ally> Hand { get;private set; }
        public List<int> PlayersMonsterTokens { get; private set; }
        public int Pearls { get; set; }
        public bool IsActivePlayer { get; set; }
        public int PlayersKeys { get; set; }
        public int TotalKeys
        {
            get
            {
                var lordsKeys = 0;
                if(PlayersLords!=null)
                foreach (Lord lord in PlayersLords)
                {
                    if (lord.IsControlingLocation == false)
                        lordsKeys++;
                }

                return PlayersKeys + lordsKeys;
            }
        }
        public bool PerformedAction { get; set; }
        private IQuestions _question;
        
        public Player(string playerName, int playerSequence)
        {
            PlayerName=playerName;
            PlayerSequence = playerSequence;
            _question = new Questions();
            PerformedAction = false;
            PlayersLords = new List<Lord>();
            AffiliatedAllies = new List<Ally>();
            PlayersLocations = new List<Location>();
            Hand = new List<Ally>();
            PlayersMonsterTokens = new List<int>();
            Pearls = 1;
            PlayersKeys = 0;
        }

        public delegate void GameCollectionChangeEventHandler(object sender, EventArgs e);
        public event GameCollectionChangeEventHandler ExplorationTrackChange;
        public event GameCollectionChangeEventHandler CourtChange;
        public event GameCollectionChangeEventHandler CouncilChange;
        public event GameCollectionChangeEventHandler PlayersLordsChange;
        public event GameCollectionChangeEventHandler PlayersAffiliatedAlliesChange;
        public event GameCollectionChangeEventHandler PlayersPearlsChange;
        public event GameCollectionChangeEventHandler PlayersKeysChange;
        public event GameCollectionChangeEventHandler PlayersTokensChange;
        public event GameCollectionChangeEventHandler PlayersLocationsChange;
        public event GameCollectionChangeEventHandler PlayersHandChange;
        

        protected virtual void OnExplorationTrackChange()
        {
            if (ExplorationTrackChange != null)
                ExplorationTrackChange(this, EventArgs.Empty);
        }
        protected virtual void OnCourtChange()
        {
            if (CourtChange != null)
                CourtChange(this, EventArgs.Empty);
        }
        protected virtual void OnCouncilChange()
        {
            if (CouncilChange != null)
                CouncilChange(this, EventArgs.Empty);
        }
        protected virtual void OnPlayersLordChange()
        {
            if (PlayersLordsChange != null)
                PlayersLordsChange(this, EventArgs.Empty);
        }
        protected virtual void OnPlayersAffiliatedAlliesChange()
        {
            if (PlayersAffiliatedAlliesChange != null)
                PlayersAffiliatedAlliesChange(this, EventArgs.Empty);
        }
        protected virtual void OnPlayersPearlsChange()
        {
            if (PlayersPearlsChange != null)
                PlayersPearlsChange(this, EventArgs.Empty);
        }
        protected virtual void OnPlayersKeysChange()
        {
            if (PlayersKeysChange != null)
                PlayersKeysChange(this, EventArgs.Empty);
        }
        protected virtual void OnPlayersTokensChange()
        {
            if (PlayersTokensChange != null)
                PlayersTokensChange(this, EventArgs.Empty);
        }
        protected virtual void OnPlayersLocationsChange()
        {
            if (PlayersLocationsChange != null)
                PlayersTokensChange(this, EventArgs.Empty);
        }
        protected virtual void OnPlayersHandChange()
        {
            if (PlayersHandChange != null)
                PlayersHandChange(this, EventArgs.Empty);
        }

        public void AddLordToCourt() //Done
        {
            if (Game.Court.Count < Game.Court.Capacity)
            {
                if (Pearls >= 1)
                {
                    Pearls--;
                    OnPlayersPearlsChange();
                    Game.Court.Add(Game.LordStack[Game.LordStack.Count - 1]);
                    OnCourtChange();
                    Game.LordStack.RemoveAt(Game.LordStack.Count - 1);
                }
                else
                    throw new ArgumentOutOfRangeException("pearlCost", "Not enough pearls.");
            }
            else
                throw new InvalidOperationException("The court is already full.");
        }

        public void Explore() //Done
        {
            bool cardWasBought = false;
            bool cardIsLast = false;

            while (Game.ExplorationTrack.Count<=Game.ExplorationTrack.Capacity)
            {
                Game.ExplorationTrack.Add(Game.AllyStack[Game.AllyStack.Count - 1]);
                OnExplorationTrackChange();
                Game.AllyStack.RemoveAt(Game.AllyStack.Count - 1);

                if (Game.ExplorationTrack.Count == Game.ExplorationTrack.Capacity)
                    cardIsLast = true;

                if (Game.ExplorationTrack[Game.ExplorationTrack.Count - 1].Race == RaceOption.monster)
                {
                   if (_question.YesNoQuestion("Do you (" + PlayerName + ") Want to fight the monster?", "A wild monster appears!"))
                    {
                        Game.DangerTrack.FightMonster(this);
                        OnPlayersKeysChange();
                        OnPlayersPearlsChange();
                        OnPlayersTokensChange();
                        break;
                    }
                    else
                    {
                        Game.DangerTrack.ProgressDanger();
                        continue;
                    }
                }
                else
                {
                    foreach (Player player in Game.AllPlayers)
                    {
                        if (player != Game.ActivePlayer)
                            if (_question.YesNoQuestion("Do you (" + player.PlayerName + ") want to buy this ally for" + player.Game.AllyCost + "?",
                                Game.ActivePlayer.PlayerName + " is exploring."))
                            {
                                if (player.BuyAlly())
                                {
                                    cardWasBought = true;
                                    break;
                                }
                                else
                                    continue;
                            }
                            else
                                continue;
                    }

                    if (cardIsLast)
                    {
                        Hand.Add(Game.ExplorationTrack[Game.ExplorationTrack.Count - 1]);
                        Game.ExplorationTrack.RemoveAt(Game.ExplorationTrack.Count - 1);
                        OnExplorationTrackChange();
                        Pearls++;
                        OnPlayersPearlsChange();
                        OnPlayersHandChange();
                        break;
                    }

                    if (cardWasBought)
                    {
                        cardWasBought = false;
                        continue;
                    }
                    else
                    {
                        cardWasBought = false;
                        if (_question.YesNoQuestion("Do you (" + PlayerName + ") take the ally?",
                            Game.ActivePlayer.PlayerName + " is exploring."))
                        {
                            Hand.Add(Game.ExplorationTrack[Game.ExplorationTrack.Count - 1]);
                            Game.ExplorationTrack.RemoveAt(Game.ExplorationTrack.Count - 1);
                            OnExplorationTrackChange();
                            OnPlayersHandChange();
                            break;
                        }
                        else
                            continue;
                    }
                }                
            }
            foreach(Ally card in Game.ExplorationTrack)
            {
                switch (card.Race)
                {
                    case RaceOption.jellyfish:
                        Game.Council[0].Add(card);
                        break;
                    case RaceOption.crabs:
                        Game.Council[1].Add(card);
                        break;
                    case RaceOption.seahorses:
                        Game.Council[2].Add(card);
                        break;
                    case RaceOption.shellfish:
                        Game.Council[3].Add(card);
                        break;
                    case RaceOption.squids:
                        Game.Council[4].Add(card);
                        break;
                    case RaceOption.monster:
                        Game.DiscardedAllies.Add(card);
                        break;
                }
            }
            OnCouncilChange();
            Game.ExplorationTrack.Clear();
            OnExplorationTrackChange();
            PerformedAction = true;
        }  

        public void BuyLord (List<Ally>cardCost, int pearlsCost, Lord lordToBuy) //Done
        {
            var racesUsed = new List<RaceOption>();
            var sumOfPoints = 0;

            foreach (Ally card in cardCost)
            {
                if(!racesUsed.Contains(card.Race))
                    racesUsed.Add(card.Race);
            }
            if (racesUsed.Count != lordToBuy.DifferentRaces)
                throw new ArgumentOutOfRangeException("cardCost", "The condition of number of different races required to buy this lord is not matched.");
            if (!racesUsed.Contains(lordToBuy.Guild))
                throw new ArgumentOutOfRangeException("cardCost", "There is no " + lordToBuy.Guild.ToString() + " race among the cards you want to pay with.");
            foreach(Ally card in cardCost)
            {
                sumOfPoints += card.Points;
            }
            if (sumOfPoints+pearlsCost < lordToBuy.PointCost)
                throw new ArgumentOutOfRangeException("cardCost", "You don't have enough points to buy this lord.");

            var lowest = LowestAllies(cardCost);
            var allyToAffiliate = _question.AllyToAffiliate(lowest);

            AffiliatedAllies.Add(allyToAffiliate);
            OnPlayersAffiliatedAlliesChange();
            cardCost.Remove(allyToAffiliate);
            Game.DiscardedAllies.AddRange(cardCost);

            PlayersLords.Add(lordToBuy);
            Game.Court.Remove(lordToBuy);
            OnCourtChange();
            if (lordToBuy.Keys == 0)
                lordToBuy.Ability();
            Hand = Hand.Except(cardCost).ToList<Ally>();
            Pearls -= pearlsCost;
            OnPlayersPearlsChange();
            PerformedAction = true;
        }

        public void TakeCouncil(List<Ally> councillStackToTake) //Done
        {
            if (councillStackToTake.Count > 0)
            {
                Hand.AddRange(councillStackToTake);
                councillStackToTake.Clear();
                OnCouncilChange();
            }
            else
                throw new ArgumentNullException("councillStackToTake", "The council stack is empty.");
            PerformedAction = true;
        }

        public bool BuyAlly() //Done
        {
            if (Pearls >= Game.AllyCost)
            {
                Game.ActivePlayer.Pearls += Game.AllyCost;
                OnPlayersPearlsChange();
                Pearls -= Game.AllyCost;
                Hand.Add(Game.ExplorationTrack[Game.ExplorationTrack.Count - 1]);
                Game.ExplorationTrack.RemoveAt(Game.ExplorationTrack.Count - 1);
                OnExplorationTrackChange();
                Game.AllyCost++;
                return true;
            }
            else
            {
                MessageBox.Show("You don't have enough pearls to buy this ally.\nThe cost is" + Game.AllyCost + ".");
                return false;
            }
        }

        public void TakeLocation() //Done
        {
            Location playersNewLocation;
            if (_question.AvailableOrStackLocations(PlayerName))
            {
                playersNewLocation = _question.LocationToChose(Game.PublicLocationsPool);
                PlayersLocations.Add(playersNewLocation);
                Game.PublicLocationsPool.Remove(playersNewLocation);
                AssignLordsToLocation();
                OnPlayersLocationsChange();
            }
            else
            {
                var LocationsToChose = new List<Location>();
                var random = new Random();
                for(int i = 0; i < _question.HowManyLocsFromStack(PlayerName); i++)
                {
                    int id = random.Next(0, Game.LocationStack.Count - 1);
                    LocationsToChose.Add(Game.LocationStack[id]);
                    Game.LocationStack.RemoveAt(id);
                }                
                playersNewLocation = _question.LocationToChose(LocationsToChose);
                PlayersLocations.Add(playersNewLocation);
                LocationsToChose.Remove(playersNewLocation);
                Game.PublicLocationsPool.AddRange(LocationsToChose);
                foreach (Location loc in LocationsToChose)
                    Game.LocationStack.Remove(loc);
                AssignLordsToLocation();
                OnPlayersLocationsChange();
            }
        }

        public void EndTurn() //Done
        {
            if (TotalKeys > 2)
                TakeLocation();
            Game.AllyCost = 1;
            PerformedAction = false;
            Game.ActivePlayer = Game.AllPlayers[(Game.AllPlayers.IndexOf(Game.ActivePlayer)+1)%Game.AllPlayers.Count];
            OnPlayersLordChange();
            OnPlayersAffiliatedAlliesChange();
            OnPlayersPearlsChange();
            OnPlayersKeysChange();
            OnPlayersTokensChange();
            OnPlayersLocationsChange();
            OnPlayersHandChange();
        }
        
        private List<Ally> LowestAllies(List<Ally> allyList) //Done
        {
            var lowest = allyList.OrderBy(ally => ally.Points).First().Points;
            var alliesWithLowestPoints = allyList.Where(ally => ally.Points == lowest).ToList();

            return alliesWithLowestPoints;
        }

        private void AssignLordsToLocation() //Done
        {
            var lordsToControl = new List<Lord>();
            foreach (Lord lord in PlayersLords)
            {
                if (lord.IsControlingLocation == false && lord.Keys > 0)
                    lordsToControl.Add(lord);
            }

            switch (PlayersKeys)
            {
                case 0:
                    lordsToControl = _question.ChoseLordsForLocation(lordsToControl, 3);
                    foreach (Lord lord in lordsToControl)
                        lord.IsControlingLocation = true;
                    break;
                case 1:
                    lordsToControl = _question.ChoseLordsForLocation(lordsToControl, 2);
                    foreach (Lord lord in lordsToControl)
                        lord.IsControlingLocation = true;
                    PlayersKeys--;
                    OnPlayersKeysChange();
                    break;
                case 2:
                    lordsToControl = _question.ChoseLordsForLocation(lordsToControl, 1);
                    foreach (Lord lord in lordsToControl)
                        lord.IsControlingLocation = true;
                    PlayersKeys -= 2;
                    OnPlayersKeysChange();
                    break;
                default:
                    PlayersKeys -= 3;
                    OnPlayersKeysChange();
                    break;
            }
        }
    }
}
